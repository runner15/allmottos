<?php $motto_get = $_GET["motto"] ?>
<?php include("php/variable.php"); ?>
<?php 
$motto_id_got = substr($motto_get, 0, strpos($motto_get, 'z')); 
if (empty($motto_id_got))
{
	header( "Location:".$website_main_url);
}
?>
<?php include("header.php") ?>
<?php include("leftside.php") ?>
<div class="span9">
	<table class="table table-bordered">
        <thead>
          <tr>
            <th>Motto</th>
            <td><?php echo $motto_id_array[$motto_id_got][0]; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>Author</th>
            <td><?php echo $motto_id_array[$motto_id_got][2]; ?></td>
          </tr>
          <tr>
            <th>Score</th>
            <td><?php echo round($motto_id_array[$motto_id_got][1], 2); echo $_COOKIE["c12"]; ?></td>
          </tr>
          <?php 
            $full_cookie = $_COOKIE["motto_votes"];
            $cookie_votes_array = explode('_', $full_cookie);
            if (!in_array($motto_id_got, $cookie_votes_array)): 
          ?>
          <tr>
            <th>Vote<!--<pre><?php print_r($_COOKIE); ?></pre>--></th>
            <td>
            	<div class="btn-toolbar" style="margin: 0;">
	              <div class="btn-group">
	                <?php
	                	for ($k=1; $k < 11; $k++) { 
	                		echo "<a href='".$website_main_url."php/vote.php?vote=".$k."&motto=".$motto_id_got."'><button class='btn'>".$k."</button></a>";
	                	}
	                ?>
	              </div>
	            </div>
    		</td>
          </tr>
      	  <?php endif; ?>
        </tbody>
    </table>
</div>

<?php include("footer.php") ?>