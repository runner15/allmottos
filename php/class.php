<?php
	include("connect.php");

	class database
	{
		public $motto;
		public $motto_array = array();
		public $rating;
		public $rating_array = array();
		public $author;
		public $author_array = array();
		public $id;
		public $id_array = array();
		public $category;
		public $score;
		private $mysqli;
		public $counter_array = array();
		public $multi_dim_values = array();
		public $multi_dim_category = array();


		function setMysqli($mysqli)
		{
			$this->mysqli = $mysqli;
		}

		function setCategory($category)
		{
			$this->category = $category;
		}

		function query_category()
		{
			if ($stmt = $this->mysqli->prepare("SELECT id, motto, score, author FROM mottos WHERE category=? ORDER BY score"))
			{
    			$stmt->bind_param("s", $this->category);
		    	$stmt->execute();
		    	$stmt->bind_result($id, $motto, $ranking, $author);
		    	while ( $stmt->fetch() ) {
			    	$this->motto_array[] = $motto;
			    	$this->rating_array[] = $ranking;
			    	$this->author_array[] = $author;
			    	$this->id_array[] = $id;
				}
		    	$stmt->close();
		    }
		}

		function multi_dim_array()
		{
			$multi_dim_values = array($this->motto_array, $this->rating_array, $this->author_array, $this->id_array);
			$counter_array = range(0,count($this->motto_array)-1);
			foreach($counter_array as $index => $key) {
			    $foreach_array = array();
			    foreach($multi_dim_values as $value) {
			        $foreach_array[] = $value[$index];
			    }
			    $multi_dim_category[$key]  = $foreach_array;
			}
			return $multi_dim_category;
		}

	}


	class mottodatabase
	{
		public $motto;
		public $motto_array = array();
		public $rating;
		public $rating_array = array();
		public $author;
		public $author_array = array();
		public $id;
		public $id_array = array();
		public $category;
		public $score;
		private $mysqli;
		public $counter_array = array();
		public $multi_dim_values = array();
		public $multi_dim_category = array();

		function setMysqli($mysqli)
		{
			$this->mysqli = $mysqli;
		}

		function query_id()
		{
			if ($stmt = $this->mysqli->prepare("SELECT id, motto, score, author FROM mottos"))
			{
		    	$stmt->execute();
		    	$stmt->bind_result($id, $motto, $ranking, $author);
		    	while ( $stmt->fetch() ) {
			    	$this->motto_array[] = $motto;
			    	$this->rating_array[] = $ranking;
			    	$this->author_array[] = $author;
			    	$this->id_array[] = $id;
				}
		    	$stmt->close();
		    }
		}

		function multi_dim_array()
		{
			$multi_dim_values = array($this->motto_array, $this->rating_array, $this->author_array);
			foreach($this->id_array as $index => $key) {
			    $foreach_array = array();
			    foreach($multi_dim_values as $value) {
			        $foreach_array[] = $value[$index];
			    }
			    $multi_dim_category[$key]  = $foreach_array;
			}
			return $multi_dim_category;
		}
	}





	/*echo "<br /><br /><b>Category Person</b><br />";
	$person_category = new database;
	$person_category->SetMysqli($mysqli);
	$person_category->SetCategory("person");
	$person_category->query_category();
	$all_values_from_category_person = $person_category->multi_dim_array();

	print_r($all_values_from_category_person);

	echo "<br /><br /><b>Category Life</b><br />";
	$life_category = new database;
	$life_category->SetMysqli($mysqli);
	$life_category->SetCategory("life");
	$life_category->query_category();
	$all_values_from_category_life = $life_category->multi_dim_array();

	print_r($all_values_from_category_life);*/
	/*Code to display array

	$counter = 0;

	while($counter < 3)
	{
		echo $all_values_from_category_person[$counter][0];
		echo $all_values_from_category_person[$counter][1];
		echo "<br /><br />";
		$counter++;
	}
	*/

