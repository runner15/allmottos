<?php
include("connect.php");
include("variable.php");
$motto_get = $_GET["motto"];
$vote_get = $_GET["vote"];

class voting
{
	public $vote;
	public $vote_get;
	private $mysqli;
	public $motto;
	public $motto_get;
	public $score;
	public $timesvoted;
	public $ip;
	public $time;
	public $date;
	public $new_timesvoted;
	public $old_cookie_value;
	public $new_cookie_value;


	function setMysqli($mysqli)
	{
		$this->mysqli = $mysqli;
	}

	function setVote($vote_get)
	{
		$this->vote = $vote_get;
	}

	function setMotto($motto_get)
	{
		$this->motto = $motto_get;
	}

	function getOldScore()
	{
		if ($stmt = $this->mysqli->prepare("SELECT score, timesvoted FROM mottos WHERE id=?"))
		{
			$stmt->bind_param("d", $this->motto);
	    	$stmt->execute();
	    	$stmt->bind_result($this->score, $this->timesvoted);
	    	$stmt->fetch();
	    	$stmt->close();
	    }
	}

	function addScore()
	{
		$this->new_score = (($this->score*$this->timesvoted)+$this->vote)/($this->timesvoted+1);
	}

	function addTimesVoted()
	{
		$this->new_timesvoted = $this->timesvoted + 1;
	}

	function updateTimesVoted()
	{
		if ($stmt = $this->mysqli->prepare("UPDATE mottos SET timesvoted=? WHERE id=?"))
		{
			$stmt->bind_param("sd", $this->new_timesvoted, $this->motto);
	    	$stmt->execute();
	    	$stmt->close();
	    }
	}

	function updateScore()
	{
		if ($stmt = $this->mysqli->prepare("UPDATE mottos SET score=? WHERE id=?"))
		{
			$stmt->bind_param("sd", $this->new_score, $this->motto);
	    	$stmt->execute();
	    	$stmt->close();
	    }
	}

	function getCookie()
	{
		$this->old_cookie_value = $_COOKIE["motto_votes"];
	}

	function updateCookie()
	{
		$this->new_cookie_value = $this->old_cookie_value."_".$this->motto;
	}

	function setCookie()
	{
		setcookie("motto_votes",$this->new_cookie_value,time() + (30 * 24 * 60 * 60), "/");
	}

	function redirect($website_main_url)
	{
		header("Location:".$website_main_url."motto.php?motto=".$this->motto."z".rand(100,999));
	}

	function addVoteRecord()
	{
		if ($stmt = $this->mysqli->prepare("INSERT INTO votes (motto, vote, ip, time, date) VALUES (?,?,?,?,?)"))
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$time = date('g:i a');
			$date = date('F j, Y');
			$stmt->bind_param("ddsss", $this->motto, $this->vote, $ip, $time, $date);
	    	$stmt->execute();
	    	$stmt->close();
	    }
	}

}
if (!ctype_digit($motto_get) ||  !ctype_digit($vote_get))
{
	header("Location:".$website_main_url."motto.php?motto=".$motto_get."z".rand(100,999));
}
elseif ( $vote_get < 0 || $vote_get > 10)
{
	header("Location:".$website_main_url."motto.php?motto=".$motto_get."z".rand(100,999));
}
else
{
	$voter = new voting;
	$voter->setMysqli($mysqli);
	$voter->setMotto($motto_get);
	$voter->setVote($vote_get);
	$voter->getOldScore();
	$voter->addScore();
	$voter->updateScore();
	$voter->addVoteRecord();
	$voter->addTimesVoted();
	$voter->updateTimesVoted();
	$voter->getCookie();
	$voter->updateCookie();
	$voter->setCookie();
	$voter->redirect($website_main_url);
}
