<?php
include("connect.php");
include("variable.php");
$motto_get = $_GET["motto"];
$vote_get = $_GET["vote"];

class voting
{
	public $vote;
	public $vote_get;
	private $mysqli;
	public $motto;
	public $motto_get;
	public $score;
	public $timesvoted;

	function setMysqli($mysqli)
	{
		$this->mysqli = $mysqli;
	}

	function setVote($vote_get)
	{
		$this->vote = $vote_get;
	}

	function setMotto($motto_get)
	{
		$this->motto = $motto_get;
	}

	function getOldScore()
	{
		if ($stmt = $this->mysqli->prepare("SELECT score, timesvoted FROM mottos WHERE id=?"))
		{
			$stmt->bind_param("d", $this->motto);
	    	$stmt->execute();
	    	$stmt->bind_result($this->score, $this->timesvoted);
	    	$stmt->fetch();
	    	$stmt->close();
	    }
	}

	function addScore()
	{
		$this->new_score = (($this->score*$this->timesvoted)+$this->vote)/($this->timesvoted+1);
	}

	function updateScore()
	{
		if ($stmt = $this->mysqli->prepare("UPDATE mottos SET score=? WHERE id=?"))
		{
			$stmt->bind_param("sd", $this->new_score, $this->motto);
	    	$stmt->execute();
	    	$stmt->close();
	    }
	}

	function setCookie()
	{
		setcookie($this->motto,'1',time() + (30 * 24 * 60 * 60));
	}

	function redirect($website_main_url)
	{
		header("Location:".$website_main_url."motto.php?motto=".$this->motto."z".rand(100,999));
	}

	function addVoteRecord()
	{
		if ($stmt = $this->mysqli->prepare("INSERT INTO votes (motto, vote, ip, time, date) VALUES (?,?,?,?,?)"))
		{
			$stmt->bind_param("sd", $this->new_score, $this->motto);
	    	$stmt->execute();
	    	$stmt->close();
	    }
	}

}
if (!ctype_digit($motto_get) ||  !ctype_digit($vote_get))
{
	header("Location:".$website_main_url."motto.php?motto=".$motto_get."z".rand(100,999));
}
elseif ( $vote_get < 0 || $vote_get > 10)
{
	header("Location:".$website_main_url."motto.php?motto=".$motto_get."z".rand(100,999));
}
	$test = new voting;
	$test->setMysqli($mysqli);
	$test->setMotto($motto_get);
	$test->setVote($vote_get);
	$test->getOldScore();
	$test->addScore();
	$test->updateScore();
	//$test->setCookie();
	$test->redirect($website_main_url);
