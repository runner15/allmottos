 <!--  Carousel - consult the Twitter Bootstrap docs at
                  http://twitter.github.com/bootstrap/javascript.html#carousel -->
          <div id="this-carousel-id" class="carousel slide"><!-- class of slide for animation -->
            <!--<ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>-->
            <div class="carousel-inner">
              <div class="item active"><!-- class of active since it's the first item -->
                <img src="http://placehold.it/850x200" alt="" />
                <div class="text-center">
                  <div class="carousel-caption">
                    <p><a class="btn btn-small btn-primary" href="#">See Related and Comment</a></p>
                  </div>
                </div>
              </div>
              <div class="item">
                <img src="http://placehold.it/850x200" alt="" />
                <div class="carousel-caption">
                  <p>Caption text here</p>
                </div>
              </div>
              <div class="item">
                <img src="http://placehold.it/850x200" alt="" />
                <div class="carousel-caption">
                  <p>Caption text here</p>
                </div>
              </div>
              <div class="item">
                <img src="http://placehold.it/850x200" alt="" />
                <div class="carousel-caption">
                  <p>Caption text here</p>
                </div>
              </div>
            </div><!-- /.carousel-inner -->
            <!--  Next and Previous controls below
                  href values must reference the id for this carousel -->
              <a class="carousel-control left" href="#this-carousel-id" data-slide="prev">&lsaquo;</a>
              <a class="carousel-control right" href="#this-carousel-id" data-slide="next">&rsaquo;</a>
          </div><!-- /.carousel -->